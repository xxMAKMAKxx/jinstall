#!/usr/bin/python

#############################################################
#                   Martin Kacmarcik                        #
#                       mkacmarc                            #
#               Red Hat Brno, PnT DevOps                    #
#                   FOR THE HORDE!!!                        #
#############################################################


import sys, getopt, re, os, os.path, urllib2, zipfile, shutil, pexpect, time, glob
#from subprocess import call

#VARIABLE DECLARATION, OS has options: linux, windows, solaris, solaris-sparc
argumentArray = {"dest" : "", "version" : 0, "update" : 0, "unlimited" : 0, "bc" : 0, "a":"64", "os" : "linux", "rh" : 0, "optimalization" : 0}
globalN = 10

#set architecture based on current one

is_64bits = sys.maxsize > 2**32
if(is_64bits):
    argumentArray["a"] = "64"
else:
    argumentArray["a"] = "32"

#will say you if requested version is in the page
def isVersionThere(string):
    #in default we suppose that the version is not there
    isThere = False
    #open url page
    url = urllib2.urlopen(string)
    #check lines if you can find it
    for lines in url.readlines():
        if(re.search(argumentArray["version"]+'u'+argumentArray["update"], lines)):
            isThere = True
    return isThere

# FUNCTION HELP:
def help():
    print('Help for automatic Oracle JDK installation Beta')
    print('--help       -> Write help')
    print('--dest       -> Destination path')
    print('--version    -> Specify major version')
    print('--update     -> Specify update')
    print('--unlimited  -> Enable Unlimited Strenght Jurisdiction Policy')
    print('--bc         -> Enable The Legion of the Bouncy Castle JCE provider')
    print('--a          -> Architecture -> 32/64, if not stated, will use your current architecture')
    print('--os         -> Choose OS, Linux is default, has options: linux, windows, solaris, solaris-sparc')
    print('--rh         -> Special Red Hat option, will do some more settings')
    print('--optimalization         -> Will not delete file after installation is done. Use this in case of multiple BC and unlimited combinations')
    print("NOTE - if error is raised (e.g. directory doesn't exist) then update doesn't exist")
# END OF HELP FUNCTION

def file_get_content(filename):
    try:
        with open(filename, 'r') as f:
            return f.read()
    except (OSError, IOError):
        exit(2)

#FUNCTION BLOG
def argumentProccessing(argv):
    global argumentArray
    helpBool = 0
    times = 0
    try:
        opts, args = getopt.getopt(argv, "", ["help", "dest=", "version=", "update=", "unlimited", "bc", "a=", "os=", "rh", "optimalization"])
    except getopt.GetoptError:
        print('Argument error: use --help to get informations about parameters.')
        sys.exit(1)
    for opt, arg in opts:
        if opt == "--help":
            helpBool = 1
        elif opt == "--dest":
            argumentArray["dest"] = arg
            times = times + 1
        elif opt == "--version":
            argumentArray["version"] = arg
            times = times + 1
        elif opt == "--update":
            argumentArray["update"] = arg
            times = times + 1
        elif opt == "--unlimited":
            argumentArray["unlimited"] = 1
        elif opt == "--bc":
            argumentArray["bc"] = 1
        elif opt == "--a":
            argumentArray["a"] = arg
        elif opt == "--rh":
            argumentArray["rh"] = 1
        elif opt == "--optimalization":
            argumentArray["optimalization"] = 1
        elif opt == "--os":
            if(arg == "windows" or arg == "solaris" or arg == "linux" or arg == "solaris-sparc"):
                argumentArray["os"] = arg
            else:
                print("Bad OS, printing help:")
                help()
                sys.exit(17)

    if (helpBool == 1):
        help()
        sys.exit(0)
    if(times < 3):
        print("Bad params, times lesser than 3")
        help()
        sys.exit(18)
    if(argumentArray["a"] != "64" and argumentArray["a"] != "32"):
        print(argumentArray["a"])
        print("Error, bad architecture.")
        help()
        sys.exit(2)

if __name__ == "__main__":
    argumentProccessing(sys.argv[1:])


#PSEUDOMAIN
architecture = ""

#Transforming 1.X version to X version
transformVersion = re.compile("^1\.[5-8]$")
if(transformVersion.match(argumentArray["version"])):
    argumentArray["version"] = re.sub("1\.([0-9])", r'\1', argumentArray["version"])

#one check to be sure
if(argumentArray["version"] == "8" and argumentArray["update"] >= "60" and argumentArray["a"] == "32" and (argumentArray["os"] == "solaris" or argumentArray["os"] == "solaris-sparc")):
    print("32 bit version of this update on version 8 and solaris OS doesn't exist.")
    sys.exit(21)

#SET THE ENDING OF THE DOWNLOAD FILE BASED ON VERSION
if(argumentArray["version"] == "7" or argumentArray["version"] == "8"):
    if(argumentArray["os"] == "windows"):
        if(argumentArray["a"] == "64"):
            architecture = "x64.exe"
        else:
            architecture = "i586.exe"
    elif(argumentArray["os"] == "solaris-sparc"):
        if(argumentArray["a"] == "64"):
            architecture = "solaris-sparcv9.tar.gz"
        else:
            architecture = "solaris-sparc.tar.gz"
    else:
        if(argumentArray["a"] == "64"):
            architecture = "x64.tar.gz"
        else:
            architecture = "i586.tar.gz"
elif(argumentArray["version"] == "6"):
    if(argumentArray["os"] == "windows"):
        if(argumentArray["a"] == "64"):
            architecture = "x64.exe"
        else:
            architecture = "i586.exe"
    elif(argumentArray["os"] == "solaris-sparc"):
        print("6th version of Java and Solaris-sparc is not yet supported by JInstall, exiting now")
        sys.exit(25)
    else:
        if(argumentArray["a"] == "64"):
            architecture = "x64.bin"
        else:
            architecture = "i586.bin"
elif(argumentArray["version"] == "5"):
    if(argumentArray["os"] == "windows"):
        print("5th version of Java and Windows is not yet supported by JInstall, exiting now")
        sys.exit(25)
    elif(argumentArray["os"] == "solaris-sparc"):
        print("5th version of Java and Solaris-sparc is not yet supported by JInstall, exiting now")
        sys.exit(25)
    else:
        if(argumentArray["a"] == "64"):
            architecture = "amd64.bin"
        else:
            architecture = "i586.bin"

#Get the ./ dir so you can always get back if needed
cwdGlobal = os.getcwd()
os.chdir(argumentArray["dest"])


#START THE PROCESS FOR VERSION 6 7 or 8
if(argumentArray["version"] == "6" or argumentArray["version"] == "7" or argumentArray["version"] == "8"):
    #Check if there is not previously downloaded zip (somebody used optimalization param)
    if(argumentArray["os"] == "solaris-sparc"):
        nameOfFile = "./jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+architecture
    #non solaris sparc OS
    else:
        nameOfFile = "./jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture
    #check if file exist
    if(os.path.isfile(nameOfFile) == True):
        shouldDownload = False
    else:
        shouldDownload = True
    if(shouldDownload):
        #find version and open url with archives of updates
        url = urllib2.urlopen("http://www.oracle.com/technetwork/java/javase/archive-139210.html")
        for lines in url.readlines():
            if re.search(r"Java SE "+str(argumentArray["version"]), lines):
                string = re.sub(r".*(<a target=\"\" href=\".*?\">Java SE "+argumentArray["version"]+"</a>).*", r'\1', lines)
                string = re.sub(r"<a target=\"\" href=\"(.*?)\">Java SE "+argumentArray["version"]+"</a>", r'\1', string)
                string = "http://www.oracle.com/"+string
                string = re.sub("[\n\r]", "", string)
        #check if update is on found page
        if(not isVersionThere(string)):
            if(isVersionThere("http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html")):
                string = "http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html"
            else:
                print("Version you requested doesn't exist, please check with available version on Oracle websites")
                sys.exit(11)
        #find update - solaris sparc has different naming convetion
        if(argumentArray["os"] == "solaris-sparc"):
            url = urllib2.urlopen(string)
            for lines in url.readlines():
                if re.search("jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+architecture, lines):
                    string = re.sub(".*\"filepath\":\"(.*?)\"\}", r'\1', lines)
                    string = re.sub("[\n\r]", "", string)
                    string = re.sub(";", "", string)
        #else for non solaris sparc versions
        else:
            url = urllib2.urlopen(string)
            for lines in url.readlines():
                if re.search("jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture, lines):
                    string = re.sub(".*\"filepath\":\"(.*?)\"\}", r'\1', lines)
                    string = re.sub("[\n\r]", "", string)
                    string = re.sub(";", "", string)
        #to make the download public - for all
        string = re.sub("/otn/", "/otn-pub/", string)

        #check if page was really found, like if JInstall want to just download HTML from above, we don't want that
        if(not re.search(argumentArray["version"]+'u'+argumentArray["update"], string)):
            print("Something went wrong, Oracle probably changed naming conventions. Please contact developers.")
            sys.exit(14)


        #download the file from oracle servers
        os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" "+string)
    else:
        print('--------------------------------------------------------------------------------')
        print('SKIPPING downloading for optimalization. Previous instance of the archive found.')
        print('--------------------------------------------------------------------------------')
    #if(argumentArray["os"] == "windows"):
    #    print("Installation of JDK done, you have chosen Windows OS, therefore you can find your exe file in dest directory, exiting now.")
    #    sys.exit(0)
    #else:
    #solaris 64 bit need download of 32 bit version first on older updates
    if((argumentArray["os"] == "solaris" or argumentArray["os"] == "solaris-sparc") and argumentArray["a"] == "64"):
        os.chdir(cwdGlobal)
        if(argumentArray["rh"] == 1):
            if(argumentArray["optimalization"] == 0):
                os.system('./JInstall.py --dest '+argumentArray["dest"]+' --os '+argumentArray["os"]+' --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --rh')
            else:
                os.system('./JInstall.py --dest '+argumentArray["dest"]+' --os '+argumentArray["os"]+' --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --rh --optimalization')
        else:
            if(argumentArray["optimalization"] == 0):
                os.system('./JInstall.py --dest '+argumentArray["dest"]+' --os '+argumentArray["os"]+' --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32')
            else:
                os.system('./JInstall.py --dest '+argumentArray["dest"]+' --os '+argumentArray["os"]+' --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --optimalization')
        os.chdir(argumentArray["dest"])
    #if we have version 7 or 8 we can just unpack the archive (except windows)
    if(argumentArray["version"] == "7" or argumentArray["version"] == "8"):
        if(argumentArray["os"] == "solaris-sparc"):
            os.system("tar zxvf jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+architecture+" >/dev/null")
        #windows is special as always
        elif(argumentArray["os"] == "windows"):
            os.system("rm tools.zip 2>/dev/null")
            os.system("mkdir jdk1."+argumentArray["version"]+".0_"+argumentArray["update"])
            os.system("7z e jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-windows-"+architecture+" -y >/dev/null")
            os.system("unzip tools.zip -d jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+" >/dev/null")
            for dirpath, dirnames, filenames in os.walk("."):
                for filename in [f for f in filenames if f.endswith(".pack")]:
                    filepath = os.path.join(dirpath, filename)
                    filepathjar = re.sub(r'(.*)\.pack', r'\1'+'.jar', filepath)
                    if(argumentArray["rh"]):
                        if(argumentArray["version"] == "7"):
                            os.system("/qa/tools/opt/amd64/jdk1.7.0/bin/unpack200 "+filepath+" "+filepathjar)
                        elif(argumentArray["version"] == 8):
                            os.system("/qa/tools/opt/amd64/jdk1.8.0/bin/unpack200 "+filepath+" "+filepathjar)

                    else:
                        os.system("unpack200 "+filepath+" "+filepathjar) #need to change it to some qa tools stuff later
            if(argumentArray["optimalization"] == 0):
                os.system("rm tools.zip 2>/dev/null")
        else:
            os.system("tar zxvf jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture+" >/dev/null")
    #if version 6 you need to set chmod and execute binary
    elif(argumentArray["version"] == "6"):
        os.system("chmod +x jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)
        if(int(argumentArray["update"]) > 20):
            os.system("./jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)
        else:
            child = pexpect.spawn("./jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)
            #child.expect("Do you agree to the above license terms? [yes or no]")
            yesBool = 0
            for item in child:
                item = item.strip()
                print(item)
                if(re.search("For inquiries please", item)):
                    yesBool = 1
                    child.interact()
                else:
                    time.sleep(0.03)
                    child.send("yes\n")
                if(re.search("Do you agree to the above license terms\?", item)):
                    child.interact()
                    break
            os.system("clear")
    if(argumentArray['optimalization'] == 0):
        if(argumentArray["os"] == "solaris-sparc"):
            os.system("rm jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+architecture)
        else:
            os.system("rm jdk-"+argumentArray["version"]+"u"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)
    print('------------------------')
    print("Installation of JDK done")
    print('------------------------')
    if(argumentArray["os"] == "linux" or argumentArray["os"] == "solaris" or argumentArray["os"] == "solaris-sparc" or argumentArray["os"] == "windows"):
        #taking care of unlimited command
        if(argumentArray["unlimited"] == 1):
            #os.chdir("jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/security")
            cwd = os.getcwd()
            #change dir to dir where files should be put
            if(int(argumentArray["update"]) < 10):
                dirOfUnlimitedFiles = "jdk1."+argumentArray["version"]+".0_0"+argumentArray["update"]+"/jre/lib/security"
            else:
                dirOfUnlimitedFiles = "jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/security"

            #Part with downloading or decising that downloading is not neccessary begins
            nameOfZip = ""
            if(int(argumentArray["version"]) == 6):
                nameOfZip = "jce_policy-6.zip"
                if(not os.path.isfile("./"+nameOfZip)):
                    os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce_policy/6/jce_policy-6.zip")
                else:
                    print('--- Skipping downloading of unlimited policy, files already found ---')
            elif(int(argumentArray["version"]) == 5):
                nameOfZip = "jce_policy-1_5_0.zip"
                if(not os.path.isfile("./"+nameOfZip)):
                    os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce_policy/1.5.0/jce_policy-1_5_0.zip")
                else:
                    print('--- Skipping downloading of unlimited policy, files already found ---')
            elif(int(argumentArray["version"]) == 7):
                nameOfZip = "UnlimitedJCEPolicyJDK7.zip"
                if(not os.path.isfile("./"+nameOfZip)):
                    os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce/7/UnlimitedJCEPolicyJDK7.zip")
                else:
                    print('--- Skipping downloading of unlimited policy, files already found ---')
            elif(int(argumentArray["version"]) == 8):
                nameOfZip = "jce_policy-8.zip"
                if(not os.path.isfile("./"+nameOfZip)):
                    os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip")
                else:
                    print('--- Skipping downloading of unlimited policy, files already found ---')
            else:
                print("This version doesn't support Unlimited Strength Jurisdiction Policy. You can have files for 5, 6, 7 or 8 version.")
                sys.exit(4)

            #start of unziping
            originalStdout = sys.stdout
            f = open(os.devnull, 'w')
            sys.stdout = f
            ########################
            z = zipfile.ZipFile(nameOfZip)
            z.extractall()
            ########################
            sys.stdout = originalStdout
            #end of unziping

            #if optimalization is not set, delete zip
            if(argumentArray["optimalization"] == 0):
                os.system("rm "+nameOfZip)
            #get name of dir with unlimited files
            unlimitedDirArray = next(os.walk('.'))[1]
            for i in range(0, unlimitedDirArray.__len__()):
                if(re.search("(UnlimitedJCEPolicy)|(jce_policy)", unlimitedDirArray[i])):
                    unlimitedDir = unlimitedDirArray[i]

            os.chmod(dirOfUnlimitedFiles+'/'+'local_policy.jar', 0776)
            os.chmod(dirOfUnlimitedFiles+'/'+'US_export_policy.jar', 0776)

            shutil.copyfile(unlimitedDir+"/local_policy.jar", dirOfUnlimitedFiles+'/'+"local_policy.jar")
            shutil.copyfile(unlimitedDir+"/US_export_policy.jar", dirOfUnlimitedFiles+'/'+"US_export_policy.jar")
            os.system("rm -rf "+unlimitedDir)
            os.chdir(cwd)
            print("---------------------------------------------------------------------")
            print("Download/Enable the Unlimited Strength Jurisdiction Policy COMPLETED")
            print("---------------------------------------------------------------------")
        #It is neccessary to check if there is not .zip file
        else:
            nameOfZip = ""
            if(int(argumentArray["version"]) == 6):
                nameOfZip = "jce_policy-6.zip"
            elif(int(argumentArray["version"]) == 5):
                nameOfZip = "jce_policy-1_5_0.zip"
            elif(int(argumentArray["version"]) == 7):
                nameOfZip = "UnlimitedJCEPolicyJDK7.zip"
            elif(int(argumentArray["version"]) == 8):
                nameOfZip = "jce_policy-8.zip"
            if(argumentArray["optimalization"] == 0):
                if(os.path.isfile('./'+nameOfZip)):
                    os.system("rm "+nameOfZip)
        #taking care of bouncy castle
        if(argumentArray["bc"] == 1):
            cwd = os.getcwd()
            url = ""
            if(int(argumentArray["update"]) < 10):
                bouncyProviderDestination = "jdk1."+argumentArray["version"]+".0_0"+argumentArray["update"]+"/jre/lib/ext"
            else:
                bouncyProviderDestination = "jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/ext"
            if(int(argumentArray["version"]) == 2):
                url = "http://www.bouncycastle.org/download/bcprov-jdk12-153.jar"
            elif(int(argumentArray["version"]) == 3):
                url = "http://www.bouncycastle.org/download/bcprov-jdk13-153.jar"
            elif(int(argumentArray["version"]) == 4):
                url = "http://www.bouncycastle.org/download/bcprov-jdk14-153.jar"
            elif(int(argumentArray["version"]) > 4):
                url = "http://www.bouncycastle.org/download/bcprov-jdk15on-154.jar"

            #download the file
            if((not os.path.isfile('bcprov-jdk15on-154.jar')) and (not os.path.isfile('bcprov-jdk14-153.jar')) and (not os.path.isfile('bcprov-jdk13-153.jar')) and (not os.path.isfile('bcprov-jdk12-153.jar'))):
                os.system("wget "+url)
            else:
                print('--- Skipping downloading of bouncy castle provider, file already found ---')
            #copy file to bouncyProviderDestination
            os.system("cp ./bcprov-jdk1* ./"+bouncyProviderDestination)

            if(int(argumentArray["update"]) < 10):
                os.chdir("jdk1."+argumentArray["version"]+".0_0"+argumentArray["update"]+"/jre/lib/security")
            else:
                os.chdir("jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/security")

            #enable the file in java.security
            os.chmod('java.security', 0776)
            file = open('java.security', 'r')
            counter = 0
            for line in file.readlines():
                if(re.search(re.compile("^security.provider.[0-9]+=", re.MULTILINE), line)):
                    counter = counter + 1
            N = counter + 1
            globalN = N

            content = file_get_content("java.security")
            if(re.search(re.compile("^security.provider."+str(counter)+"=.*?$", re.MULTILINE), content)):
                #stringos = re.sub(re.compile("[\s\S]*?(security.provider."+str(counter)+"=.*?\n)[\s\S]*", re.MULTILINE), r'\1', content)
                content = re.sub("([\s\S]*?)(security.provider."+str(counter)+"=.*\n)([\s\S]*)", r'\1'r'\2'+"security.provider."+str(N)+"=org.bouncycastle.jce.provider.BouncyCastleProvider\n"+r'\3', content)
            file = open('java.security', 'w')
            file.write(content)
            os.chdir(cwd)
            if(argumentArray["optimalization"] == 0):
                if((os.path.isfile('bcprov-jdk15on-154.jar')) or (os.path.isfile('bcprov-jdk14-153.jar')) or (os.path.isfile('bcprov-jdk13-153.jar')) or (os.path.isfile('bcprov-jdk12-153.jar'))):
                    os.system("rm bcprov-jdk1*")

            print("---------------------------------------------------------------------")
            print("Download/enable The Legion of the Bouncy Castle JCE provider COMPLETED")
            print("---------------------------------------------------------------------")
                #It is neccessary to check if there is not .zip file
        else:
            if(argumentArray["optimalization"] == 0):
                if((os.path.isfile('bcprov-jdk15on-154.jar')) or (os.path.isfile('bcprov-jdk14-153.jar')) or (os.path.isfile('bcprov-jdk13-153.jar')) or (os.path.isfile('bcprov-jdk12-153.jar'))):
                    os.system("rm bcprov-jdk1*")
################################
#  VERSION 5 - BUGGED AS HELL  #
################################
#note: 5 versions has many bugs and even though it should be functional, still there can be a lot of bugs
elif(argumentArray["version"] == "5"):
    #find version and open url with archives of updates
    url = urllib2.urlopen("http://www.oracle.com/technetwork/java/javase/archive-139210.html")
    if(int(argumentArray["update"]) < 10):
        argumentArray["update"] = "0"+argumentArray["update"]
    for lines in url.readlines():
        if re.search(r"Java SE "+str(argumentArray["version"]), lines):
            string = re.sub(r".*(<a target=\"\" href=\".*?\">Java SE "+argumentArray["version"]+"</a>).*", r'\1', lines)
            string = re.sub(r"<a target=\"\" href=\"(.*?)\">Java SE "+argumentArray["version"]+"</a>", r'\1', string)
            string = "http://www.oracle.com/"+string
            string = re.sub("[\n\r]", "", string)

    #find update
    url = urllib2.urlopen(string)
    for lines in url.readlines():
        if re.search("jdk-1_"+argumentArray["version"]+"_0_"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture, lines):
            string = re.sub(".*\"filepath\":\"(.*?)\"\}", r'\1', lines)
            string = re.sub("[\n\r]", "", string)
            string = re.sub(";", "", string)

    string = re.sub("/otn/", "/otn-pub/", string)

    #download the binary
    os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" "+string)

    #set chmod to execute for binary
    os.system("chmod +x jdk-1_"+argumentArray["version"]+"_0_"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)
    #start the child process with pexecept so script can controll the installation process (and accept Oracle terms)
    child = pexpect.spawn("./jdk-1_"+argumentArray["version"]+"_0_"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)
    #child.expect("Do you agree to the above license terms? [yes or no]")
    yesBool = 0
    for item in child:
        item = item.strip()
        print(item)
        if(re.search("For inquiries please", item)):
            yesBool = 1
            child.interact()
        else:
            time.sleep(0.03)
            child.send("yes\n")
        if(re.search("Do you agree to the above license terms\?", item)):
            child.interact()
            break


    os.system("clear")
    print("Installation of JDK done")
    os.system("rm jdk-1_"+argumentArray["version"]+"_0_"+argumentArray["update"]+"-"+argumentArray["os"]+"-"+architecture)



    #taking care of unlimited command
    if(argumentArray["unlimited"] == 1):
        cwd = os.getcwd()
        os.chdir("jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/security")
        nameOfZip = ""
        if(int(argumentArray["version"]) == 6):
            os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce_policy/6/jce_policy-6.zip")
            nameOfZip = "jce_policy-6.zip"
        elif(int(argumentArray["version"]) == 5):
            os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce_policy/1.5.0/jce_policy-1_5_0.zip")
            nameOfZip = "jce_policy-1_5_0.zip"
        elif(int(argumentArray["version"]) == 7):
            os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce/7/UnlimitedJCEPolicyJDK7.zip")
            nameOfZip = "UnlimitedJCEPolicyJDK7.zip"
        elif(int(argumentArray["version"]) == 8):
            os.system("wget --no-check-certificate --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip")
            nameOfZip = "jce_policy-8.zip"
        else:
            print("This version doesn't support Unlimited Strength Jurisdiction Policy. You can have files for 5, 6, 7 or 8 version.")
            sys.exit(4)
        with zipfile.ZipFile(nameOfZip) as zip:
            originalStdout = sys.stdout
            f = open(os.devnull, 'w')
            sys.stdout = f
            zip.extractall()
            sys.stdout = originalStdout
        os.system("rm "+nameOfZip)
        unlimitedDir = next(os.walk('.'))[1]
        os.chmod('local_policy.jar', 0776)
        os.chmod('US_export_policy.jar', 0776)
        shutil.copyfile(unlimitedDir[0]+"/local_policy.jar", "local_policy.jar")
        shutil.copyfile(unlimitedDir[0]+"/US_export_policy.jar", "US_export_policy.jar")
        os.system("rm -rf "+unlimitedDir[0])
        os.chdir(cwd)
        print("---------------------------------------------------------------------")
        print("Download/Enable the Unlimited Strength Jurisdiction Policy COMPLETED")
        print("---------------------------------------------------------------------")
    #taking care of bouncy castle
    if(argumentArray["bc"] == 1):
        cwd = os.getcwd()
        url = ""
        os.chdir("jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/ext")
        if(int(argumentArray["version"]) == 2):
            url = "http://www.bouncycastle.org/download/bcprov-jdk12-153.jar"
        elif(int(argumentArray["version"]) == 3):
            url = "http://www.bouncycastle.org/download/bcprov-jdk13-153.jar"
        elif(int(argumentArray["version"]) == 4):
            url = "http://www.bouncycastle.org/download/bcprov-jdk14-153.jar"
        elif(int(argumentArray["version"]) > 4):
            url = "http://www.bouncycastle.org/download/bcprov-jdk15on-153.jar"

        #download the file
        os.system("wget "+url)
        os.chdir(cwd)
        os.chdir("jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/security")

        #enable the file in java.security
        os.chmod('java.security', 0776)
        file = open('java.security', 'r')
        counter = 0
        for line in file.readlines():
            if(re.search(re.compile("^security.provider.[0-9]+=", re.MULTILINE), line)):
                counter = counter + 1
        N = counter + 1

        content = file_get_content("java.security")
        if(re.search(re.compile("^security.provider."+str(counter)+"=.*?$", re.MULTILINE), content)):
            #stringos = re.sub(re.compile("[\s\S]*?(security.provider."+str(counter)+"=.*?\n)[\s\S]*", re.MULTILINE), r'\1', content)
            content = re.sub("([\s\S]*?)(security.provider."+str(counter)+"=.*\n)([\s\S]*)", r'\1'r'\2'+"security.provider."+str(N)+"=org.bouncycastle.jce.provider.BouncyCastleProvider\n"+r'\3', content)
        file = open('java.security', 'w')
        file.write(content)
        print("---------------------------------------------------------------------")
        print("Download/enable The Legion of the Bouncy Castle JCE provider COMPLETED")
        print("---------------------------------------------------------------------")
else:
    print("Version is not supported. This script support 5, 6, 7 or 8 version of JDK")
    sys.exit(18)
###################################
#taking care of RH special config #
###################################
if(argumentArray["rh"] == 1):

    #set port
    cwd = os.getcwd()
    #change dir
    if(int(argumentArray["update"]) < 10):
        os.chdir("jdk1."+argumentArray["version"]+".0_0"+argumentArray["update"]+"/jre/lib/security")
    else:
        os.chdir("jdk1."+argumentArray["version"]+".0_"+argumentArray["update"]+"/jre/lib/security")

    #enable the file in java.security
    os.chmod('java.policy', 0776)
    content = file_get_content('java.policy')
    content = re.sub(re.compile('java\.net\.SocketPermission \"localhost\:0\"'), 'java.net.SocketPermission \"localhost:1024-\"', content)
    javaPolicy = open('java.policy', 'w')
    javaPolicy.write(content)
    javaPolicy.close()


    #put bouncy castle as first
    if(argumentArray["bc"] == 1):
        content = file_get_content("java.security")
        pomocneN = globalN

        content = re.sub(re.compile('\n(security\.provider\.1[\s\S]*?)security\.provider\.([0-9]+)=org\.bouncycastle\.jce\.provider\.BouncyCastleProvider'), r'\nsecurity.provider.0=org.bouncycastle.jce.provider.BouncyCastleProvider\n\1', content)
        for i in range (1, globalN+1):
            content = re.sub ('\nsecurity\.provider\.'+str(pomocneN-1)+'=', '\nsecurity.provider.'+str(pomocneN)+'=', content)
            pomocneN = pomocneN - 1

        javaSecurity = open('java.security', 'w')
        javaSecurity.write(content)
        javaSecurity.close()

    os.chdir(cwd)



print("\n\n-------------------------------------------------")
print("------------------INSTALLATION-------------------")
print("--------------------COMPLETED--------------------")
print("-------------------------------------------------")
