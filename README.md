
```

  _____           _           _     ______                    
 |  __ \         (_)         | |   |  ____|                   
 | |__) | __ ___  _  ___  ___| |_  | |__ _ __ ___ _   _  __ _ 
 |  ___/ '__/ _ \| |/ _ \/ __| __| |  __| '__/ _ \ | | |/ _` |
 | |   | | | (_) | |  __/ (__| |_  | |  | | |  __/ |_| | (_| |
 |_|   |_|  \___/| |\___|\___|\__| |_|  |_|  \___|\__, |\__,_|
                _/ |                               __/ |      
               |__/                               |___/       
```

													v. 1.2

Using Python 2.7.8

Usage:

python Jinstasll.py --help --dest=destPath --version=versionNumber --update=updateNumber --unlimited --bc --a=32/64 --os=OS

Use help to get more info.

Tested on: Ubuntu 14.04, Fedora 21, Rhel 5

Info: I will be working on Freya soon, will fix a lot of bugs... but u know, soon™ :D

1.1 changelog:
-Added --rh option for some RH specific settings
-Fixed bugs when Freya didn't exit after an error and continued with errors
-Added newest versions of Java
-Architecture default is now the one of host PC
-Added --os parameter, now you can download various OS versions of Java (namely: Linux, Windows, Solaris, Solaris-sparc), all architectures, all combination of bc and ulimited
-Added some coments, lot of minor bug fixes
-A lot of text was hidden so it's now much more clear how the install is proccessing

1.2 changelog:
-Freya will now check if versions exist, will not download HTML file and raise error
-Freya is now able to handle new versions that are on special page

In future I plan to reorganize Freya into objects, so it's easier to use and more object-oriented like.