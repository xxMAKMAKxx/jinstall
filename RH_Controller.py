#!/usr/bin/python

#############################################################
#                   Martin Kacmarcik                        #
#                       mkacmarc                            #
#               Red Hat Brno, PnT DevOps                    #
#                   FOR THE HORDE!!!                        #
#############################################################

# This file serve as a control script for Red Hat PnT DevOps Core team. It is using automatic Java installer - JInstall to peform tasks.
# I have decided to create a controll script because PnT DevOps requirements are completely different from the purpose of JInstall script.
#                                                                                                               - Martin Kacmarcik 2015


import sys, getopt, re, os, os.path, urllib2, zipfile, shutil, pexpect, time
#from subprocess import call

#VARIABLE DECLARATION, OS has options: linux, windows, solaris, solaris-sparc
argumentArray = {"dest" : "", "version" : 0, "update" : 0, "unlimited" : 0, "bc" : 0, "a":"64", "os" : "linux", "symlinks" : False}

#set architecture based on current one

is_64bits = sys.maxsize > 2**32
if(is_64bits):
    argumentArray["a"] = "64"
else:
    argumentArray["a"] = "32"

# FUNCTION HELP:
def help():
    print('Help for automatic Oracle JDK installation Beta')
    print('Need to be run under sudo rights! (creating directories in root /qa/tools/ folder...')
    print('--help       -> Write help')
    print('--version    -> Specify major version')
    print('--update     -> Specify update')
    print('--a          -> Architecture -> 32/64, 64 is default')
    print('--os         -> Choose OS, Linux is default, has options: linux, windows, solaris, solaris-sparc')
    print('NOTE!!! If not stated (architecture and os) it will do all combinations')
    print('--symlinks     -> Will create simlinks')
    print("NOTE - if error is raised (e.g. directory doesn't exist) then update doesn't exist")
# END OF HELP FUNCTION

def file_get_content(filename):
    try:
        with open(filename, 'r') as f:
            return f.read()
    except (OSError, IOError):
        exit(2)

#FUNCTION BLOG
def argumentProccessing(argv):
    global argumentArray
    helpBool = 0
    times = 0
    try:
        opts, args = getopt.getopt(argv, "", ["help", "nosymlinks", "dest=", "version=", "update=", "unlimited", "bc", "a=", "os="])
    except getopt.GetoptError:
        print('Argument error: use --help to get informations about parameters.')
        sys.exit(1)
    for opt, arg in opts:
        if opt == "--help":
            helpBool = 1
        elif opt == "--version":
            argumentArray["version"] = arg
            times = times + 1
        elif opt == "--update":
            argumentArray["update"] = arg
            times = times + 1
        elif opt == "--a":
            argumentArray["a"] = arg
        elif opt == "--symlinks":
            argumentArray["symlinks"] = True
        elif opt == "--os":
            if(arg == "windows" or arg == "solaris" or arg == "linux" or arg == "solaris-sparc"):
                argumentArray["os"] = arg
            else:
                print("Bad OS, printing help")
                help()
                sys.exit(17)

    if (helpBool == 1):
        help()
        sys.exit(0)
    if(times < 2):
        print("Bad params, times lesser than 2")
        help()
        sys.exit(18)
    if(argumentArray["a"] != "64" and argumentArray["a"] != "32"):
        print(argumentArray["a"])
        print("Error, bad architecture.")
        help()
        sys.exit(2)

if __name__ == "__main__":
    argumentProccessing(sys.argv[1:])

cwd = os.getcwd()


#######################
# LINUX SECTION START #
#######################


#call JInstall for each instance

#Linux first
#32 bit version of Linux - with all combinations of BC and unlimited
os.system('./JInstall.py --dest /qa/tools/opt --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os linux --rh --bc --unlimited --optimalization' )
os.system('mv /qa/tools/opt/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')
os.system('./JInstall.py --dest /qa/tools/opt --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os linux --rh --bc --optimalization' )
os.system('mv /qa/tools/opt/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('./JInstall.py --dest /qa/tools/opt --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os linux --rh --unlimited --optimalization' )
os.system('mv /qa/tools/opt/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('./JInstall.py --dest /qa/tools/opt --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os linux --rh' )
#64 bit version of Linux - all combinations of BC and unlimited
os.system('./JInstall.py --dest /qa/tools/opt/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh --bc --unlimited --optimalization' )
os.system('mv /qa/tools/opt/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')
os.system('./JInstall.py --dest /qa/tools/opt/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh --bc --optimalization' )
os.system('mv /qa/tools/opt/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('./JInstall.py --dest /qa/tools/opt/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh --unlimited --optimalization' )
os.system('mv /qa/tools/opt/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('./JInstall.py --dest /qa/tools/opt/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh' )

if(argumentArray["symlinks"] == True):
    #create simlinks for LINUX

    #32 bit version of LINUX
    os.chdir('/qa/tools/opt/')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')

    #64 bit version of LINUX

    os.chdir('./amd64')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')

#######################
# LINUX SECTION END   #
#######################


#########################
# SOLARIS SECTION START #
#########################



#64 bit version of Solaris - all combinations of BC and unlimited
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_x86_64 --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris --rh --bc --unlimited --optimalization' )
os.system('mv /qa/tools/opt/solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_x86_64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris --rh --bc --optimalization' )
os.system('mv /qa/tools/opt/solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_x86_64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris --rh --unlimited --optimalization' )
os.system('mv /qa/tools/opt/solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_x86_64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris --rh' )

#solaris special links
os.chdir('/qa/tools/opt/solaris10_x86')

os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"])
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')

os.chdir(cwd)

#SOLARIS 11 special simlinks
# Location - solaris11_x86
os.chdir('/qa/tools/opt/solaris11_x86')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"])
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')

# Location - solaris11_x86_64
os.chdir('/qa/tools/opt/solaris11_x86_64')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"])
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('ln -sfn ../solaris10_x86_64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')

#create simlinks for Solaris
if(argumentArray["symlinks"] == True):
    #64 bit version of Solaris

    os.chdir('/qa/tools/opt/solaris10_x86_64')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')

    #32 bit links for Solaris
    os.chdir('/qa/tools/opt/solaris10_x86')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')


    #SOLARIS 11 special simlinks
    # Location - solaris11_x86
    os.chdir('/qa/tools/opt/solaris11_x86')


    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')
    # Location - solaris11_x86_64
    os.chdir('/qa/tools/opt/solaris11_x86_64')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')


#########################
# SOLARIS SECTION END   #
#########################

###############################
# SOLARIS SPARC SECTION START #
###############################

os.chdir(cwd)

#64 bit version of Solaris - all combinations of BC and unlimited
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_sparc --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris-sparc --rh --bc --unlimited --optimalization' )
os.system('mv /qa/tools/opt/solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_sparc/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris-sparc --rh --bc --optimalization' )
os.system('mv /qa/tools/opt/solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_sparc/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris-sparc --rh --unlimited --optimalization' )
os.system('mv /qa/tools/opt/solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('./JInstall.py --dest /qa/tools/opt/solaris10_sparc/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os solaris-sparc --rh' )


#links for solaris sparc v11

os.chdir('/qa/tools/opt/solaris11_sparc')

os.system('ln -sfn ../solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"])
os.system('ln -sfn ../solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('ln -sfn ../solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('ln -sfn ../solaris10_sparc/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')

#links for solaris sparc v10
if(argumentArray["symlinks"] == True):
    os.chdir('/qa/tools/opt/solaris10_sparc')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')

    #links for solaris sparc v11

    os.chdir('/qa/tools/opt/solaris11_sparc')

    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' jdk1.'+argumentArray["version"]+'.0')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC jdk1.'+argumentArray["version"]+'.0.BC')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited jdk1.'+argumentArray["version"]+'.0.unlimited')
    os.system('ln -sfn jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC jdk1.'+argumentArray["version"]+'.0.unlimited.BC')

###############################
# SOLARIS SPARC SECTION END   #
###############################

###############################
# WINDOWS SECTION STARTS      #
###############################

os.chdir(cwd)

#32 bit version of Windows - with all combinations of BC and unlimited
os.system('./JInstall.py --dest /qa/tools/opt/windows/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os windows --rh --bc --unlimited --optimalization' )
os.system('mv /qa/tools/opt/windows/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/windows/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')
os.system('./JInstall.py --dest /qa/tools/opt/windows/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os windows --rh --bc --optimalization' )
os.system('mv /qa/tools/opt/windows/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/windows/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('./JInstall.py --dest /qa/tools/opt/windows/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os windows --rh --unlimited --optimalization' )
os.system('mv /qa/tools/opt/windows/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/windows/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('./JInstall.py --dest /qa/tools/opt/windows/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 32 --os windows --rh' )
#64 bit version of Windows - all combinations of BC and unlimited
os.system('./JInstall.py --dest /qa/tools/opt/windows/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh --bc --unlimited --optimalization' )
os.system('mv /qa/tools/opt/windows/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/windows/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited.BC')
os.system('./JInstall.py --dest /qa/tools/opt/windows/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh --bc --optimalization' )
os.system('mv /qa/tools/opt/windows/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/windows/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.BC')
os.system('./JInstall.py --dest /qa/tools/opt/windows/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh --unlimited --optimalization' )
os.system('mv /qa/tools/opt/windows/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+' /qa/tools/opt/windows/amd64/jdk1.'+argumentArray["version"]+'.0_'+argumentArray["update"]+'.unlimited')
os.system('./JInstall.py --dest /qa/tools/opt/windows/amd64/ --update '+argumentArray["update"]+' --version '+argumentArray["version"]+' --a 64 --os linux --rh' )

###############################
# WINDOWS SECTION END         #
###############################




print("""
 ____   ___  _   _ _____
|  _ \ / _ \| \ | | ____|
| | | | | | |  \| |  _|
| |_| | |_| | |\  | |___
|____/ \___/|_| \_|_____|

""")
